#ifndef MPU6050_jusax23_H
#define MPU6050_jusax23_H

#include "Arduino.h"
#include "Wire.h"

#define MPU6050_ADDR         0x68
#define MPU6050_SMPLRT_DIV   0x19
#define MPU6050_CONFIG       0x1a
#define MPU6050_GYRO_CONFIG  0x1b
#define MPU6050_ACCEL_CONFIG 0x1c
#define MPU6050_WHO_AM_I     0x75
#define MPU6050_PWR_MGMT_1   0x6b
#define MPU6050_TEMP_H       0x41
#define MPU6050_TEMP_L       0x42

class MPU6050{
  public:

  MPU6050(TwoWire &w);
  MPU6050(TwoWire &w, float aC, float gC);

  void begin();

  void setGyroOffsets(float x, float y, float z);

  void writeMPU6050(byte reg, byte data);
  byte readMPU6050(byte reg);

  int16_t getRawAccX(){ return rawAccX; };
  int16_t getRawAccY(){ return rawAccY; };
  int16_t getRawAccZ(){ return rawAccZ; };

  int16_t getRawTemp(){ return rawTemp; };

  int16_t getRawGyroX(){ return rawGyroX; };
  int16_t getRawGyroY(){ return rawGyroY; };
  int16_t getRawGyroZ(){ return rawGyroZ; };

  float getTemp(){ return temp; };

  float getAccX(){ return accX; };
  float getAccY(){ return accY; };
  float getAccZ(){ return accZ; };

  void resetAccOffsetWatch() { maxAccX = 0; maxAccY = 0; maxAccZ = 0; minAccX = 0; minAccY = 0; minAccZ = 0; };

  float calcAccXoffset() { return (maxAccX + minAccX)/2; };
  float calcAccYoffset() { return (maxAccY + minAccY)/2; };
  float calcAccZoffset() { return (maxAccZ + minAccZ)/2; };

  float setAccXoffset(float x, float y, float z){accXoffset = x; accYoffset = y; accZoffset = z; };

  float getGyroX(){ return gyroX; };
  float getGyroY(){ return gyroY; };
  float getGyroZ(){ return gyroZ; };

	void calcGyroOffsets(bool console = false, uint16_t delayBefore = 1000, uint16_t delayAfter = 3000);

  float getGyroXoffset(){ return gyroXoffset; };
  float getGyroYoffset(){ return gyroYoffset; };
  float getGyroZoffset(){ return gyroZoffset; };

  void restGyroOffsetGlide(){gyroXoffsetGlide = 0; gyroYoffsetGlide = 0;gyroZoffsetGlide = 0;};

  float getGyroXoffsetGlide() { return gyroXoffsetGlide; };
  float getGyroYoffsetGlide() { return gyroYoffsetGlide; };
  float getGyroZoffsetGlide() { return gyroZoffsetGlide; };

  void update();

  float getAccAngleX(){ return angleAccX; };
  float getAccAngleY(){ return angleAccY; };

  float getGyroAngleX(){ return angleGyroX; };
  float getGyroAngleY(){ return angleGyroY; };
  float getGyroAngleZ(){ return angleGyroZ; };

  float getAngleX(){ return angleX; };
  float getAngleY(){ return angleY; };
  float getAngleZ(){ return angleZ; };

  private:

  TwoWire *wire;

  int16_t rawAccX, rawAccY, rawAccZ, rawTemp,
  rawGyroX, rawGyroY, rawGyroZ;

  float gyroXoffset, gyroYoffset, gyroZoffset;

  float gyroXoffsetGlide, gyroYoffsetGlide, gyroZoffsetGlide;

  float temp, accX, accY, accZ, gyroX, gyroY, gyroZ;

  float maxAccX, maxAccY, maxAccZ, minAccX, minAccY, minAccZ;

  float accXoffset = 0.0, accYoffset = 0.0, accZoffset = 0.0;

  float angleGyroX, angleGyroY, angleGyroZ,
  angleAccX, angleAccY, angleAccZ;

  float angleX, angleY, angleZ;

  float interval;
  long preInterval;
  long nowInterval;

  float accCoef, gyroCoef;
};

#endif
